# ReadMark

find an email whose subject is 'mail subject' from a particular sender named sender@mail.com and then do some further actions with that mail

 You can have multiple search-keys of the form
 
 search-key  = "ALL" / "ANSWERED" / "BCC" SP astring /
              "BEFORE" SP date / "BODY" SP astring /
              "CC" SP astring / "DELETED" / "FLAGGED" /
              "FROM" SP astring / "KEYWORD" SP flag-keyword /
              "NEW" / "OLD" / "ON" SP date / "RECENT" / "SEEN" /
              "SINCE" SP date / "SUBJECT" SP astring /
              "TEXT" SP astring / "TO" SP astring /
              "UNANSWERED" / "UNDELETED" / "UNFLAGGED" /
              "UNKEYWORD" SP flag-keyword / "UNSEEN" /
              "DRAFT" / "HEADER" SP header-fld-name SP astring /
              "LARGER" SP number / "NOT" SP search-key /
              "OR" SP search-key SP search-key /
              "SENTBEFORE" SP date / "SENTON" SP date /
              "SENTSINCE" SP date / "SMALLER" SP number /
              "UID" SP sequence-set / "UNDRAFT" / sequence-set /
              "(" search-key *(SP search-key) ")"
              
